liste = [1,2,3,4,4,4,5,6]
def plus_longue_serie(votes):
    """
    pre: votes est un tableau(list) de votes
    post : renvoie i tel que votes[i] est le premier vote de la plus longue
    suite de votes identiques
    """
    
    compteur = 0
    compteurMax = 0
    var1 = 0
    for i in range(len(votes)):
        
        try:
            if votes[i] == votes[i+1]:           
                compteur += 1
                if compteur > compteurMax:
                    compteurMax = compteur
                    var1 = ((i-compteur)+1)
            else:
                compteur = 0
        except:
            continue
    i = var1
    return i
print(plus_longue_serie(liste))
