liste = [1,1,2,3,4,4,4,5,5,5,6,6,6,6]

def top3(votes):
    """
    pre :votes est un tableau(list) de votes
    post : renvoie un tableau t (trié de façon décroissante) de
        la longueur des trois plus longues suites de votes identiques
    """
    
    compteur = 0
    compteurMax = 0
    var1 = 0
    array1 = list()
    for i in range(len(votes)):
        
        try:
            if votes[i] == votes[i+1]:           
                compteur += 1
                if compteur > compteurMax:
                    compteurMax = compteur
                    var1 = ((i-compteur)+1)
            else:
                array1.append(compteur+1)
                compteur = 0
                
                
        except:
            array1.append(compteur+1)
            continue
    array1.sort(reverse = True)
    t = list()
    for i in range(3):
        t.append(array1[i])
    return t
    

print(top3(liste))




